# Trivia

[Trivia❓](https://en.wikipedia.org/wiki/Trivia) is a form of game or mind sport in which players attempt to answer 
questions correctly about a certain or variety of subjects. The game can be played with other players or by yourself.

The project includes the server, programmed in C++, and the client, programmed in C#.

## Technologies

The back-end (server) is created with:
* [C++](https://isocpp.org/)
* [C](https://www.gnu.org/software/gnu-c-manual/)
* [Python](https://www.python.org/)
* [Sqlite3](https://www.sqlite.org/index.html)
* [nlohmann/json](https://github.com/nlohmann/json)

The front-end (client) is created with:
* [C#](https://docs.microsoft.com/en-us/dotnet/csharp/)
* [WPF](https://docs.microsoft.com/en-us/dotnet/desktop/wpf/?view=netdesktop-5.0)
* [Newtonsoft.Json](https://www.newtonsoft.com/json)

## Usage

Inorder to play, you will need to run the server, and then connect with the clients.
You can run the server .exe ( ```Server.exe``` ) or run it from the Solution file. Same goes for the clients.
After connecting to the server, you will need to login:

<img src="https://i.imgur.com/zuKZnt1.png" alt="Login Screen">
<br><br/>
<br><br/>

If you dont have an account, you could sign up by pressing the button that says "Sign Up",
where you will be moved to the sign up window:

<img src="https://i.imgur.com/eUMj4n0.png" alt="Signup Screen">

* Password     - should be more than 8 characters long, contain both lowercase and uppercase letters, at least one number and at least one special character.
* Email        - should be a valid email address
* Street       - should contain only lowercase and uppercase letters
* Apt          - should contain only numbers
* City         - should contain only lowercase and uppercase letters
* Phone Number - should be a valid phone number
* Birth Date   - should be a valid date
<br><br/>
<br><br/>


When logging-in, you will approach the main menu screen, where you can choose from one of the options:

<img src="https://i.imgur.com/7yTKpPh.png" alt="Main Menu Screen">
<br><br/>
<br><br/>

The game screen, where you will answer the questions and play, contains the amount of time the player has left, 
the amount of questions he answered, and the amount of correct answers:

<img src="https://i.imgur.com/8tqFGnt.png" alt="Game Screen">
<br><br/>

Have fun playing our game! 😄

## 
